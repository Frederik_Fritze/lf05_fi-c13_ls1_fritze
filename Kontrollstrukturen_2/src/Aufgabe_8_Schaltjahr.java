import java.util.Scanner;

public class Aufgabe_8_Schaltjahr {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Geben Sie eine Jahreszahl ein: ");
		int jahr = myScanner.nextInt();
		
		if (jahr >= 1582 && jahr % 4 == 0 && !((jahr % 100 == 0) && !(jahr % 400 == 0)))
		{
			System.out.print(jahr + " ist ein Schaltjahr!");
		}
		else
			System.out.print(jahr + " ist kein Schaltjahr!");
		myScanner.close();	
	}

	}


