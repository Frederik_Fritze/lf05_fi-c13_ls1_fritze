import java.util.Scanner;

public class Aufgabe_5_Taschenrechner {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Geben Sie eine Zahl ein: ");
		
		double zahl1 = myScanner.nextDouble();
		
		System.out.print("Geben Sie eine weitere Zahl ein: ");
		
		double zahl2 = myScanner.nextDouble();
		
		System.out.print("Geben sie den gewünschten rechenoperator an (+ - * /): ");
		
		char operator = myScanner.next().charAt(0);
		double ergebnis;
		
		switch(operator) 
		{
		case '+':
			ergebnis = zahl1 + zahl2;
			
			System.out.print("Das Ergebnis ist " + ergebnis);
			break;
			
		case '-':
			ergebnis = zahl1 - zahl2;
			
			System.out.print("Das Ergebnis ist " + ergebnis);
			break;
			
		case '*':
			ergebnis = zahl1 * zahl2;
			
			System.out.print("Das Ergebnis ist " + ergebnis);
			break;
			
		case '/':
			ergebnis = zahl1 / zahl2;
			
			System.out.print("Das Ergebnis ist " + ergebnis);
			break;
			
			default:
				System.out.print("Ungültiger Operator");
			break;
			
		}
		myScanner.close();
	}
}
