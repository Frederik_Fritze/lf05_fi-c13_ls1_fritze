﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = tastatur.nextDouble();
		System.out.print("Bitte geben Sie die gewünschte Anzahl an Tickets ein: ");
	    byte anzahlTickets = tastatur.nextByte();
	    zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
	    return zuZahlenderBetrag;
	}
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in); 
	    double eingezahlterGesamtbetrag; 
		double eingeworfeneMünze;
		eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.print( "Noch zu zahlen: " );
	    	   System.out.printf("%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		return eingezahlterGesamtbetrag;
		
	}
	
	public static void fahrkartenAusgeben( ) {System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
   	 
   	 double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.print("Der Rückgabebetrag in Höhe von ");
 	   System.out.printf("%.2f", rückgabebetrag);
 	   System.out.println(" EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }
   	   
      }

	
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag; //Datentypen double für Kommazahlen bei Euro beträgen
       double eingeworfeneMünze;
       double rückgabebetrag;
       byte anzahlTickets; //Byte für Ganzzahlige Ticketanzahl
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben( );
       rueckgeldAusgeben( eingezahlterGesamtbetrag, zuZahlenderBetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}